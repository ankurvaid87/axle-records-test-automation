package apiTests;

import api.UsersAPI;
import org.json.simple.parser.ParseException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.Token;

import java.io.IOException;

public class UserTests {
    private UsersAPI usersAPI;
    private Token token;

    @BeforeTest
    public void initialize() throws IOException, ParseException {
        usersAPI = new UsersAPI();
        token = new Token();
    }

    @Test
    @Parameters({"env"})
    public void createUserTest(String env) throws Exception {
        usersAPI.createUser(token.getJWT(), env);
    }
}
