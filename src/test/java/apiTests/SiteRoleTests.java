package apiTests;

import api.SiteRoleAPI;
import api.UsersAPI;
import org.json.simple.parser.ParseException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import utils.Token;

import java.io.IOException;
import java.util.Map;

public class SiteRoleTests {
    private UsersAPI usersAPI;
    private Token token;
    private SiteRoleAPI siteRoleAPI;

    @BeforeTest
    public void initialize() throws IOException, ParseException {
        usersAPI = new UsersAPI();
        token = new Token();
        siteRoleAPI = new SiteRoleAPI();
    }

    @Test
    @Parameters({"env"})
    public void addCustomerSiteRole(String env) throws Exception {
        Map<String, String> idTokenMap = token.getJWT();
        String idTokenString = idTokenMap.get("idtoken");

        usersAPI.createUser(idTokenMap, env);
        String userId = usersAPI.getCreatedUserId();

        siteRoleAPI.addCustomerSiteRole(idTokenString, userId, env);
    }
}
