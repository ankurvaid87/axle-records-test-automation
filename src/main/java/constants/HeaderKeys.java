package constants;

public class HeaderKeys {

    public static final String ACCESS_TOKEN = "access_token";
    public static final String ACCEPT = "Accept";
    public static final String CONTENT_TYPE = "Content-Type";
}
