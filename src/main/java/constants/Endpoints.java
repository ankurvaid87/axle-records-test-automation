package constants;

public class Endpoints {

    public static final String BASE_URL_DEV = "BaseURLDEV";
    public static final String BASE_URL_QA = "BaseURLQA";
    public static final String SHOPS = "Shops";
    public static final String CREATE_USER = "CreateUser";
    public static final String GET_USER = "GetUser";
    public static final String ADD_SITE_ROLE = "AddSiteRole";
    public static final String GET_USER_SITE_ROLE = "GetUserSiteRole";
}
