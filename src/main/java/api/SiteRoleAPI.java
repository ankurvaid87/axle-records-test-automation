package api;

import io.restassured.http.Method;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import utils.FileUtil;

import java.io.IOException;
import java.util.*;

public class SiteRoleAPI{
    Logger logger;
    Response response;

    public SiteRoleAPI() throws IOException, ParseException {
        logger = LogManager.getLogger(SiteRoleAPI.class);
    }

    /**
     * Assigns a Site Role "Customer" to a given userId
     * @param userId
     * @param env
     * @throws Exception
     */
    public void addCustomerSiteRole(String idToken, String userId, String env) throws Exception {
        response = new Request(FileUtil.getBaseURL(env))
                .setMethod(Method.GET)
                .setEndPoint(FileUtil.getAddSiteRoleEndpoint())
                .setHeader("Authorization", idToken)
                .setQueryParam("site_role_id", "2")
                .setQueryParam("test_mode", "true")
                .execute();
        logger.info("Response Status Code Received : " + response.statusCode());
        Assert.assertTrue(response.statusCode() == 201, "API endpoint did not return status code 201");
        Assert.assertTrue(getUserSiteRole(idToken, env).equalsIgnoreCase("customer"),
                "Site role for created user did not match Customer");
    }

    /**
     * Assigns a Site Role "Vendor" to a given userId
     * @param userId
     * @param env
     * @throws Exception
     */
    public void addVendorSiteRole(String userId, String env) throws Exception {
        response = new Request(FileUtil.getBaseURL(env))
                .setMethod(Method.GET)
                .setEndPoint(FileUtil.getAddSiteRoleEndpoint())
                .setQueryParam("site_role_id", "3")
                .setQueryParam("user_id", userId)
                .execute();

        Assert.assertTrue(response.statusCode() == 201);
    }

    /**
     * Returns site role assigned to a user (with Auth Header as idToken)
     * @param idToken
     * @param env
     * @return
     * @throws Exception
     */
    public String getUserSiteRole(String idToken, String env) throws Exception {
        response = new Request(FileUtil.getBaseURL(env))
                .setMethod(Method.GET)
                .setEndPoint(FileUtil.getUserSiteRoleEndpoint())
                .setHeader("Authorization", idToken)
                .setQueryParam("test_mode", "true")
                .execute();

        Assert.assertTrue(response.statusCode() == 200, "Get Site Role endpoint did not return status code 200");

        logger.debug("Response received from Get Site Role Endpoint : " + response.body().jsonPath().prettyPrint());

        List<HashMap<String, String>> jsonResponse = response.jsonPath().getList("$");
        String siteRole = jsonResponse.get(0).get("name");
        System.out.println(" name is -----------> "+ siteRole);

        return siteRole;
    }
}
