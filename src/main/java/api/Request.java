package api;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.properties.PropertiesConfiguration;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

public class Request {

    private RequestSpecification httpRequest;
    private String baseUrl;
    private String relativeUrl;
    private Method requestMethod;
    private Logger logger;

    public Request(String baseUrl) throws IOException {
        this.baseUrl = baseUrl;
        RestAssured.baseURI = baseUrl;
        httpRequest = RestAssured.given();
        logger = LogManager.getLogger(Request.class);
    }

    /**
     * Sets the request endpoint
     *
     * @param relativeUrl request endpoint
     * @return Request object
     */
    public Request setEndPoint(String relativeUrl) {
        this.relativeUrl = relativeUrl;
        return this;
    }

    /**
     * Sets the request method
     *
     * @param method request method
     * @return Request object
     */
    public Request setMethod(Method method) {
        requestMethod = method;
        return this;
    }

    /**
     * Sets the request header
     *
     * @param name  name of header
     * @param value value of header
     * @return Request object
     */
    public Request setHeader(String name, String value) {
        this.httpRequest.header(name, value);
        logger.debug("Header -> " + name + " : " + value);
        return this;
    }

    /**
     * Sets the request header
     *
     * @param header map of key,value pairs
     * @return Request object
     */
    public Request setHeader(Map<String, String> header) {
        for (Map.Entry<String, String> entry : header.entrySet()) {
            this.setHeader(entry.getKey(), entry.getValue());
        }
        return this;
    }

    /**
     * Sets the request queryParam
     *
     * @param name  name of queryParam
     * @param value value of queryParam
     * @return Request object
     */
    public Request setQueryParam(String name, String value) {
        this.httpRequest.queryParam(name, value);
        logger.debug("Query Param -> " + name + " : " + value);
        return this;
    }

    /**
     * Sets the request queryParams
     *
     * @param queryParams map of key,value pairs
     * @return Request object
     */
    public Request setQueryParams(Map<String, String> queryParams) {
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            this.setQueryParam(entry.getKey(), entry.getValue());
        }
        return this;
    }


    /**
     * Sets the request body as form params
     *
     * @param params Hashmap of form params
     * @return Request object
     */
    public Request setBody(Map<String, ?> params) {
        httpRequest.formParams(params);
        return this;
    }

    /**
     * Sets Basic Auth
     * @param username
     * @param password
     * @return
     */
    public Request setBasicAuth(String username, String password) {
        httpRequest.auth().basic(username, password);
        logger.debug("Basic Auth : \nUsername : " + username + "\nPassword : " + password);
        return this;
    }

    /**
     * Executes the http(s) request
     *
     * @return Response Object
     */
    public Response execute() {
        logger.info("Sending " + requestMethod + " request to : " + baseUrl+relativeUrl);
        return httpRequest.request(requestMethod, relativeUrl);
    }
}
