package api;

import io.restassured.http.Method;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import utils.FileUtil;
import utils.Token;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UsersAPI {
    private Token token = new Token();
    private Response response;
    private Logger logger;
    private Map<String, String> userInfo;

    public UsersAPI() throws IOException, ParseException {
        logger = LogManager.getLogger(UsersAPI.class);
    }

    /**
     * Creates a new User with random username, email and JWT idToken
     * @param idToken
     */
    public void createUser(Map<String, String> idToken, String env) throws Exception {
        logger.debug("JWT idToken : " + idToken);
        response = new Request(FileUtil.getBaseURL(env))
                .setEndPoint(FileUtil.getCreteUserEndpoint())
                .setQueryParam("test_mode", "true")
                .setMethod(Method.POST)
                .setBody(idToken)
                .execute();
        logger.debug("Response Body : " + response.body().jsonPath().prettyPrint());
        logger.info("Response code received : " + response.statusCode());

        userInfo = new HashMap<String, String>();
        userInfo = response.body().jsonPath().getMap(""); //Adding created user's information to userInfo

        Assert.assertTrue(this.response.statusCode() == 201, "API endpoint did not return status code 201");
    }

    /**
     * Gets users using uid from DB
     * TODO: Add actual test to verify existing user.
     * @param uid
     */
    public void getExistingUser(String uid, String env) throws Exception {
        response = new Request(FileUtil.getBaseURL(env))
                .setEndPoint(FileUtil.getUserEndpoint())
                .setMethod(Method.GET)
                .execute();

        Assert.assertTrue(this.response.statusCode() == 200, "API endpoint did not return status code 200");
        // TODO: Add assertion for verifying uid
    }

    /**
     * Returns uid for created user
     * @return
     */
    public String getCreatedUserId(){
        return userInfo.get("uid");
    }
}
