package utils;

import io.jsonwebtoken.Jwts;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Token {
    public static String testUserName;
    public static String testUserEmail;
    public static String userId;
    public static String idToken;

    public String getTimestampNow(){
        SimpleDateFormat formatter= new SimpleDateFormat("ddMMyyyyHHmmss");
        Date date = new Date(System.currentTimeMillis());

        return formatter.format(date);
    }

    /**
     * Generates a name for test User with timestamp appended
     * @return
     */
    public String generateTestUserName(){
        testUserName = "TestUser" + "_" + getTimestampNow();
        return testUserName;
    }

    /**
     * Generates an email for Test User with mailinator.com
     * @return
     */
    public String generateTestUserEmail(){
        testUserEmail = testUserName + "@mailinator.com";
        return testUserEmail;
    }

    /**
     * Generates unique userId with timestamp appended
     * @return
     */
    public String generateUniqueUserId(){
        userId = "TestUserSubId" + getTimestampNow();

        return userId;
    }

    /**
     * Generates a String JWT
     * @return
     */
    public String getStringIdToken(){
        Date expDate = Date.from(LocalDateTime.now().plusMinutes(5).atZone(ZoneId.systemDefault()).toInstant());

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", generateTestUserName());
        map.put("iss", "https://securetoken.google.com/axle-records-firebase");
        map.put("aud", "axle-records-firebase");
        map.put("sub", generateUniqueUserId());
        map.put("iat", getTimestampNow());
        map.put("exp", String.valueOf(expDate.getTime()));
        map.put("email", generateTestUserEmail());

        idToken = Jwts.builder()
                .setHeaderParam("alg", "RS256")
                .setHeaderParam("typ", "JWT")
                .setClaims(map)
                .compact();

        return idToken.toString();
    }

    /**
     * Converts a String JWT to a JSONObject
     * @return
     */
    public Map<String, String> getJWT(){
        Map<String, String> jwt = new HashMap<String, String>();
        jwt.put("idtoken", getStringIdToken());

        return jwt;
    }
}
