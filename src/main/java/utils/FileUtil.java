package utils;

import constants.Endpoints;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.HashMap;

public class FileUtil {

    /**
     * Parses JSON from resources and returns a JSON Object
     * @param fileName
     * @return JsonObject
     * @throws IOException
     * @throws ParseException
     */
    public static HashMap<String, String> readJSON(String fileName) throws IOException, ParseException {
        File file = new File(ClassLoader.getSystemClassLoader().getResource(fileName).getFile());
        Reader fileReader = new FileReader(file);
        JSONParser jsonParser = new JSONParser();
        return (HashMap<String, String>)jsonParser.parse(fileReader);
    }

    /**
     * Reads APIEndpoints.json and returns a HashMap containing all API Endpoints
     * @return API Endpoints (HashMap)
     * @throws IOException
     * @throws ParseException
     */
    public static HashMap<String, String> getAllAPIEndpoints() throws IOException, ParseException {
        return readJSON("APIEndpoints.json");
    }

    /**
     * Returns Base URL
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getBaseURL(String env) throws Exception {
        if(env.equalsIgnoreCase("dev") || env.isEmpty() || env.isBlank() || env.equalsIgnoreCase("${env}")){
            return getAllAPIEndpoints().get(Endpoints.BASE_URL_DEV);
        }else if(env.equalsIgnoreCase("qa")){
            return getAllAPIEndpoints().get(Endpoints.BASE_URL_QA);
        }else{
            throw new Exception("Please check env passed while executing tests with maven...!!" +
                    " e.g. mvn clean -Denv=<NameOfEnvironment> test OR mvn clean -Denv=<NameOfEnvironment> install");
        }
    }

    /**
     * Returns Create User Endpoint
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getCreteUserEndpoint() throws IOException, ParseException {
        return getAllAPIEndpoints().get(Endpoints.CREATE_USER);
    }

    /**
     * Returns GET User Endpoint
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getUserEndpoint() throws IOException, ParseException {
        return getAllAPIEndpoints().get(Endpoints.GET_USER);
    }

    /**
     * Returns Add Site Role Endpoint
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getAddSiteRoleEndpoint() throws IOException, ParseException {
        return getAllAPIEndpoints().get(Endpoints.ADD_SITE_ROLE);
    }

    /**
     * Returns Get User Site Role Endpoint
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static String getUserSiteRoleEndpoint() throws IOException, ParseException {
        return getAllAPIEndpoints().get(Endpoints.GET_USER_SITE_ROLE);
    }

}
